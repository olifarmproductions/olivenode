![](https://bytebucket.org/olifarmproductions/oliveweb/raw/04d8602f0b5d5c578e9b87eccc41f236b3716a07/meta/logo/olive_logo_banner.png?token=bcded4eab03b26e89933abb3ed0bae4504888583)

-------------------

# Olive Node
## Contents
+ [About](#markdown-header-about)
+ [Installation](#markdown-header-installation)
    - [JAR](#markdown-header-jar)
    - [Dependencies](#markdown-header-dependencies)
    - [Build from sources](#markdown-header-build-from-sources)
        * [Intellij](#markdown-header-intellij)
        * [Gradle](#markdown-header-gradle)
        * [Netbeans](#markdown-header-netbeans)
+ [Configuration](#markdown-header-configuring-the-node-app)
    - [Datasource](#markdown-header-configuring-an-external-datasource)
    - [Rabbit Server](#markdown-header-configuring-a-rabbit-server)
    - [Sftp Server](#markdown-header-configuring-a-sftp-server)
    - [Miscellaneous](#markdown-header-miscellaneous)
+ [Known issues and troubleshooting](#markdown-header-known-issues-and-troubleshooting)

---------------
## About

Installing, configuring and running course grained simulation software can be a complex process which can be very intimidating for non tech-savvy users. So much so in fact that most of these users wouldn’t even consider using these packages.  This is a shame because researchers could benefit from being able to run relatively complex protein simulations without having to resort to expert help.
Our solution to this problem is a software package called Olive. Olive includes a web interface for running these kinds of simulations and managing the results.  This web interface significantly cuts down on the expertise needed to run course grained simulations because the configuration and setup of the software has been done by and administrator. This way the end user does not have to concern him or herself with installing and configuring all the needed packages and dependencies. This way the user can focus on just running the simulations and processing the results.


This repo contains the node application of Olive.

--------------
## Installation

### JAR
Download the latest OliveNode [here](https://bitbucket.org/olifarmproductions/olivenode/downloads). Make sure the configuration is valid and you have Java 8 installed. Then run as follows:

    java -jar OliveNode<version>.jar

### Dependencies
The application is written in Java 8 using the Java Spring framework to facilitate a lot of the features. When building from sources Gradle 2.9 can be used to donwload and install all the dependancies automaticly. As stated previously Olive is built the [Spring framework](https://spring.io/).

### Build from sources

To build the project OliveCore must be added as a jar to the libs folder if it is not yet present. OliveCore can be found [here](https://bitbucket.org/olifarmproductions/olivecore) 

##### Intellij

Simply clone the project and import it into Intellij. Then link the gradle root and set it to use the 
'default gradle wrapper'. For Intellij usage see the [manual](https://www.jetbrains.com/idea/help/working-with-gradle-projects.html).
Then open the gradle tab and build the project by pressing the blue refresh button. If the global SDK is not set then
this must be [set](https://www.jetbrains.com/idea/help/configuring-global-project-and-module-sdks.html) to the java 8 JDK.
If these things have been configured the project can be run.

##### Netbeans

For running the project in Netbeans the Gradle plugin must first be installed. The plugin can be found [here](http://plugins.netbeans.org/plugin/44510/gradle-support).
Once installed go to Team > Git > Clone and clone the repo. For more information see [this](https://netbeans.org/kb/docs/ide/git.html). 
Then let gradle finish building the project and start the app by running "OliveJobReceiverApplication"

##### Command line

To build the project using the commandline you can use gradle. Spring has an excelent [tutorial](https://spring.io/guides/gs/gradle/) on how to do this so
I wont try to explain it myself. Since the gradle file and sources have already been made you can skip straight to
the executing part.

------------------------
## Configuring the node app
To configure the application the file application.properties is used. The repo contains a template applications
properties files named application.template. Simply create a file called application.properties in the resources folder
and fill in the gaps when building. To override the default properties file (which is highly reccomended) simply create a folder
called config in the folder where the jar is located and make a file called 'application.properties' in the config folder. Add all
the settings you want to change in this file. Then (re) start Olive Node. 

#### Configuring a new application

To add an application to olive simply add the following lines to the configuration file for each app you want to add.

    # Application name must match the name of the application in OliveWeb
    app.<application name in lowercase> = <path to executable>
    
    # Optionally if the application needs to run with python3 for example
    # Otherwise leave the path blank. The setting itself must be provided.
    app.<application name in lowercase>.execute_with = <path to executable>
    
The application names "example" and "example2" must be the same as the names in OliveWeb. If this is not the case the application won't work.

Example:

    # Example
    app.example = /home/finch/applications/example.py
    app.example.execute_with = /usr/bin/python3
    app.example2 = /home/finch/applications/example2.sh
    app.example2.execute_with = /usr/bin/bash



#### Configuring a Rabbit server

    # Enable if server is not on localhost
    spring.rabbitmq.host=<host>
    spring.rabbitmq.username=<user>
    spring.rabbitmq.password=<password>
    spring.rabbitmq.port=5672

#### Configuring a SFTP server
    # Set the SSH module only to log warnings
    logging.level.com.jcraft.jsch=WARN
    
    ftp.host=<host>
    ftp.username=<user>
    ftp.password=<password>
    ftp.external_storage_root=<Olive's working directory on the ftp server>

#### Miscellaneous settings
    # The folder on the local system to use as storage
    system.local_storage_root=/homes/obbakker/OLIVE_FTP/olive/lcl
    # The logging level of the ssh library
    logging.level.com.jcraft=WARN



------------
## Known issues and troubleshooting

> Olive node does start but doesn't work properly
    
Make sure the node is running with Java 8


> Olive throws an AMQP connect exception

Make sure the AMQP broker is correctly configured and running. How to configure a broker can be found [here](#markdown-header-configuring-a-rabbit-server)

> An configured application does not work.

Make sure ALL the nodes in the network are properly configured. One typo or misplaced space can cause issues. Check the configuration section to see how to configure Olive Node