/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.job;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.node.system.SystemStatus;
import com.olifarm.olive.node.system.SystemStatusChecker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.AmqpConnectException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Service used for polling the rabbit server for new jobs.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class JobListener {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * {@link JobProcessor} object used for processing a job.
     */
    @Autowired
    private JobProcessor jobProcessor;

    /**
     * {@link SystemStatusChecker} object used for checking if the system is available to receive a new job.
     */
    @Autowired
    private SystemStatusChecker systemStatusChecker;

    /**
     * RabbitTemplate used for communicating with the rabbitmq server.
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * Main loop of the application. Starts a listener for new jobs, when a new job has been received it processes the
     * job. Also respond to the olive server by sending status updates to the rabbitmq server.
     *
     * @throws InterruptedException when the system process gets interrupted
     * @throws IOException          when a file cannot be accessed
     */
    public void startQueueListener() throws InterruptedException, IOException {

        String jobRequests = "jobRequests";
        String jobResponses = "jobResponses";
        String jobFinished = "jobFinished";
        String errorQueue = "errorQueue";

        //Listen for messages every pollingInterval in milliseconds
        boolean run = true;
        int pollingInterval = 2000;

        while (run) {

            // Check if the system is ready to receive a new job_utils
            SystemStatus systemStatus = systemStatusChecker.checkStatus();

            if (systemStatus == SystemStatus.AVAILABLE) {

                // If the system is ready check if a job_utils is available
                try {
                    Object data = rabbitTemplate.receiveAndConvert(jobRequests);

                    if (data != null) {
                        // Send a status update to the server
                        rabbitTemplate.convertAndSend(jobResponses, data);
                        Thread.sleep(1000);
                        Job job = (Job) data;
                        try {
                            // If there is a job call the jobProcessor
                            jobProcessor.processJob(job);

                            // Send a status update to the server
                            rabbitTemplate.convertAndSend(jobFinished, job);
                        } catch (Exception e) {
                            e.printStackTrace();
                            logger.error(e.getMessage());
                            rabbitTemplate.convertAndSend(errorQueue, job);
                        }
                    }

                    // Set the interval to 2 seconds
                    pollingInterval = 2000;

                } catch (AmqpConnectException e) {
                    // When a connection cannot be made set the interval to 10 seconds
                    logger.warn("Could not connect to rabbit server");
                    pollingInterval = 10000;

                } catch (MessageConversionException e) {
                    logger.error("Could not deserialize object");

                } catch (IllegalStateException e) {
                    logger.error("Could not deserialize object");
                }

            }
            // Pause the thread for the specifed polling interval
            Thread.sleep(pollingInterval);
        }
    }
}
