/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.job;


import com.olifarm.olive.io.FilenameService;
import com.olifarm.olive.io.RemoteFileService;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;
import com.olifarm.olive.node.io.LocalFileServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Service used for processing a job.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class JobProcessor {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * Object implementing {@link RemoteFileService} used for interacting with the ftp server.
     */
    @Autowired
    private RemoteFileService remoteFileService;

    @Autowired
    private SystemService systemService;

    @Autowired
    private FilenameService filenameService;

    @Autowired
    private LocalFileServiceImpl localFileService;


    /**
     * Processes and execute a job.
     *
     * @param job the job to process
     * @return the completed job
     * @throws InterruptedException when the system process gets interrupted
     * @throws IOException          when a file cannot be accessed
     */
    public Job processJob(Job job) throws InterruptedException, IOException {

        logger.info("Job processing: " + job);
        // Prepare the system process
        this.prepareSystemProcess(job);

        logger.info("Prepared ");
        // Start the process and retrieve the exit code
        int exitCode = systemService.startSystemProcess(job);
        logger.info("Completed ");

        // Set the status of the job according to the exit code
        job.setStatus(JobStatus.convertExitCode(exitCode));

        return job;
    }

    /**
     * Prepares a system process from a job by getting and setting all the required input files and directories.
     *
     * @return {@link CliApplication} object containing the local paths
     * @throws IOException when a file cannot be accessed
     */
    private void prepareSystemProcess(Job job) throws IOException {

        // Open a ftp connection
        remoteFileService.open();

        logger.info("Preparing job: " + job);

        // Download the files from the ftp server and set them to the local filesystem
        this.downloadInputFiles(job);

        // Close the ftp connection
        remoteFileService.close();

        this.createLocalOutputFilenames(job);
    }


    /**
     * System process done.
     *
     * @param logFiles the log files
     * @param job      the job
     * @throws IOException the io exception
     */
    public void systemProcessDone(Map<ParameterType, File> logFiles, Job job) throws IOException {

        remoteFileService.open();

        try {
            this.uploadOutputFiles(job);

        } catch (IOException e){
            logger.warn("Could not upload output files");
        }

        try {
            this.uploadLogAndZipFiles(logFiles, job);

        } catch (IOException e) {
            logger.warn("Could not upload log files");

        }


        remoteFileService.close();


    }

    /**
     * Create log files map.
     *
     * @param job the job
     * @return the map
     * @throws IOException the io exception
     */
    public Map<ParameterType, File> createLogFiles(Job job) throws IOException {

        Map<ParameterType, File> files = new HashMap<>();

        // Construct logfiles
        for (Parameter parameter : job.getApp().getParameters()) {

            switch (parameter.getType()) {
                case STD_OUT:
                case STD_ERR:
                    String localLogFile = filenameService.makeLocalOutputDir(job) + "/" + parameter.getFilename();
                    File file = new File(localLogFile);
                    files.put(parameter.getType(), file);
                    localFileService.mkdir(file);

                    parameter.setLocalFilePath(localLogFile);
                    break;
                case OUTPUT_ZIP_FILE:
                    String localZipFile = filenameService.makeLocalOutputDir(job) + "/" + parameter.getFilename();
                    parameter.setLocalFilePath(localZipFile);
            }

        }
        return files;
    }

    private void createLocalOutputFilenames(Job job) {

        for (Parameter parameter : job.getApp().getParameters()) {
            if (parameter.getType() == ParameterType.OUTPUT_DIR) {
                parameter.setArguments(Collections.singletonList(filenameService.makeLocalOutputDir(job)));
            }

            if (parameter.getType() == ParameterType.OUTPUT_FILE) {

                String filename = parameter.getFilename();
                String outputFilePath = filenameService.makeLocalOutputDir(job) + "/" + filename;
                parameter.setArguments(Collections.singletonList(outputFilePath));
                parameter.setLocalFilePath(outputFilePath);

            }
        }
    }

    /**
     * Downloads the input files for a Job on the local file system, and stores the path in the copy of app.
     *
     * @param job the job
     * @throws IOException  when a file cannot be accessed
     */
    private void downloadInputFiles(Job job) throws IOException {

        for (Parameter parameter : job.getApp().getParameters()) {

            if (parameter.getType() == ParameterType.INPUT_FILE) {

                // Get the external path for the input file
                String ftpInputFilePath = parameter.getRemoteFilePath();

                // Get the internal path for the input file
                String localInputFilePath = filenameService.makeLocalInputDir(job) + "/" + parameter.getFilename();

                // Get the file from the ftp server
                remoteFileService.getFile(ftpInputFilePath, localInputFilePath);

                // Set the parameter to the local file (for commandline query)
                parameter.setArguments(Collections.singletonList(localInputFilePath));

                // For local use (in code)
                parameter.setLocalFilePath(localInputFilePath);
            }
        }
    }


    /**
     * Uploads the output files for a job to the ftp server.
     *
     * @param job the job
     * @throws IOException when a file cannot be accessed
     */
    private void uploadOutputFiles(Job job) throws IOException {

        String remoteOutputDir =  filenameService.makeRemoteOutputDir(job);
        for (Parameter parameter : job.getApp().getParameters()) {

            // If the type is an output file
            if (parameter.getType() == ParameterType.OUTPUT_FILE) {

                InputStream inputSteam = new FileInputStream(new File(parameter.getLocalFilePath()));
                String outputPath = remoteOutputDir + "/" + parameter.getFilename();
                remoteFileService.setFile(inputSteam, outputPath);

                parameter.setRemoteFilePath(outputPath);
            }
        }
    }


    /**
     * Uploads the std out and std err files to the ftp server.
     *
     * @param localFiles Map containing the path to the stdout and stderr files
     * @param job the job
     * @throws IOException  when a file cannot be accessed
     */
    private void uploadLogAndZipFiles(Map<ParameterType, File> localFiles, Job job) throws IOException {

        // Upload logfiles
        for (Parameter parameter : job.getApp().getParameters()) {

            if (parameter.getType() == ParameterType.STD_OUT ||
                    parameter.getType() == ParameterType.STD_ERR) {

                remoteFileService.mkdir(localFiles.get(parameter.getType()).getParent());
                remoteFileService.setFile(new FileInputStream(localFiles.get(parameter.getType())),
                        parameter.getRemoteFilePath());

            } else if (parameter.getType() == ParameterType.OUTPUT_ZIP_FILE) {
                localFileService.zipDirectory(filenameService.makeLocalOutputDir(job), parameter.getLocalFilePath());

                remoteFileService.mkdir(parameter.getRemoteFilePath());
                remoteFileService.setFile(new FileInputStream(parameter.getLocalFilePath()),
                        parameter.getRemoteFilePath());
            }
        }

    }


}
