/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.job;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.ParameterType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Author(s):     Olivier Bakker
 * Student id(s): 330595
 * Email(s):      o.b.bakker@st.hanze.nl
 * Institute:     Hanze University of Applied Sciences, bioinformatics department
 * Project:       olivejobreceiver
 * Package:       com.olifarm.job
 * Created on:    8-1-2016
 * All rights reserved.
 */
@Service
public class SystemService {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private Environment env;

    @Autowired
    private JobProcessor jobProcessor;

    /**
     * Start system process int.
     *
     * @param job the job
     * @return the int
     * @throws IOException          the io exception
     * @throws InterruptedException the interrupted exception
     */
    public int startSystemProcess(Job job) throws IOException, InterruptedException {

        ProcessBuilder processBuilder;

        CliApplication app = job.getApp();

        String applicationPath = env.getProperty("app." + app.getName().toLowerCase());

        // For windows compatibility
        String applicationExecutor = env.getProperty("app." + app.getName().toLowerCase() + ".execute_with");

        List<String> commandLineQuery = new ArrayList<>();

        if (!applicationExecutor.equals("")) {
            String[] executorList = applicationExecutor.split(",");
            commandLineQuery.addAll(Arrays.asList(executorList));
        }

        commandLineQuery.add(applicationPath);
        commandLineQuery.addAll(app.paramsToClString());

        processBuilder = new ProcessBuilder(commandLineQuery);

        Map<ParameterType, File> logFiles = jobProcessor.createLogFiles(job);

        processBuilder.redirectOutput(logFiles.get(ParameterType.STD_OUT));
        processBuilder.redirectError(logFiles.get(ParameterType.STD_ERR));

        // Build & run the system process
        Process process = processBuilder.start();

        process.waitFor();

        int exitCode = process.exitValue();

        jobProcessor.systemProcessDone(logFiles, job);

        return exitCode;
    }



}