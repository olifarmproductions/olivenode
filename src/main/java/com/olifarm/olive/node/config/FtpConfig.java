/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

/**
 * Class used to configure the sftp server
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Configuration
public class FtpConfig {

    /**
     * Hostname of the sftp server. Retrieved from application properties.
     */
    @Value("${ftp.host}")
    private String host;

    /**
     * Username on the sftp server. Retrieved from application properties.
     */
    @Value("${ftp.username}")
    private String username;

    /**
     * Password for the user on the sftp server. Retrieved from application properties.
     */
    @Value("${ftp.password}")
    private String password;

    /**
     * Configure the SftpSessionFactory with the correct parameters.
     *
     * @return the configured session factory
     */
    @Bean
    public DefaultSftpSessionFactory defaultSftpSessionFactory() {
        DefaultSftpSessionFactory defaultSftpSessionFactory = new DefaultSftpSessionFactory();
        defaultSftpSessionFactory.setHost(host);
        defaultSftpSessionFactory.setUser(username);
        defaultSftpSessionFactory.setPassword(password);
        defaultSftpSessionFactory.setAllowUnknownKeys(true);
        return defaultSftpSessionFactory;
    }

}
