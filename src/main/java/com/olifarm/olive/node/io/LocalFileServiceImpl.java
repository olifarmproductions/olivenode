/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.io;

import com.olifarm.olive.io.LocalFileService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * The type Local file service.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class LocalFileServiceImpl implements LocalFileService {

    @Override
    public OutputStream getFile(String s, String s1) throws IOException {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    public void setFile(InputStream inputStream, String path) throws IOException {
        this.mkdir(path);

        Path outputPath = Paths.get(path);

        Files.copy(inputStream, outputPath, StandardCopyOption.REPLACE_EXISTING);
        inputStream.close();
    }

    @Override
    public void mkdir(String path) throws IOException {
        File file = new File(path);
        this.mkdir(file);
    }

    @Override
    public void mkdir(File file) throws IOException {
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            if (!file.createNewFile()) {
                throw new IOException("Could not create file");
            }
        }
    }

    @Override
    public boolean hasDir(String path) throws IOException {
        throw new UnsupportedOperationException("not yet implemented");
    }

    /**
     * List all the files in a folder and ad them to a zip archive.
     *
     * @param inputFolder the input folder
     * @param zipFile     the zip file
     * @throws IOException the io exception
     */
    public void zipDirectory(String inputFolder, String zipFile) throws IOException {

        List<String> inputFiles = new ArrayList<>();

        inputFiles = this.generateFileList(inputFolder, new File(inputFolder), inputFiles);
        this.zipFiles(inputFiles, inputFolder, zipFile);

    }

    /**
     * Zip all the files in a list.
     *
     * @param inputFiles  the input files
     * @param inputFolder the input folder
     * @param zipFile     output ZIP file location
     * @throws IOException the io exception
     */
    public void zipFiles(List<String> inputFiles, String inputFolder, String zipFile) throws IOException {

        byte[] buffer = new byte[1024];

        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zos = new ZipOutputStream(fos);

        for (String file : inputFiles) {

            ZipEntry ze = new ZipEntry(file);
            zos.putNextEntry(ze);

            FileInputStream in = new FileInputStream(inputFolder + File.separator + file);

            int len;

            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
        }

        zos.closeEntry();
        //remember close itgenerateZipEntry(sourceFolder, node.getAbsoluteFile().toString()
        zos.close();


    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     *
     * @param sourceFolder the source folder
     * @param node         file or directory
     * @param fileList     the file list
     * @return the list
     */
    public List<String> generateFileList(String sourceFolder, File node, List<String> fileList ) {

        //add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(sourceFolder, node.getAbsoluteFile().toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(sourceFolder, new File(node, filename), fileList);
            }
        }

        return fileList;

    }

    /**
     * Format the file path for zip
     *
     * @param file file path
     * @return Formatted file path
     */
    private String generateZipEntry(String sourceFolder, String file) {
        return file.substring(sourceFolder.length() + 1, file.length());
    }
}
