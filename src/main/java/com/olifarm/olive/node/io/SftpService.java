/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.io;

import com.olifarm.olive.io.RemoteFileService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * Service used for communicating with the ftp server. Implements {@link RemoteFileService}.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class SftpService implements RemoteFileService {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * SftpSession factory used for getting new ftp sessions.
     */
    @Autowired
    private DefaultSftpSessionFactory defaultSftpSessionFactory;

    /**
     * SftpSession object used for interacting with the ftp server.
     */
    private SftpSession ftpSession;

    @Override
    public void open() {
        ftpSession = defaultSftpSessionFactory.getSession();
        logger.info("Opened ftp session: " + ftpSession);
    }

    @Override
    public void close(){
        ftpSession.close();
        logger.info("Closed ftp session: " + ftpSession);
        ftpSession = null;
    }

    @Override
    public OutputStream getFile(String inputUrl, String outputPath) throws IOException {
        File file = new File(outputPath);

        if(!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }

        OutputStream outputStream = new FileOutputStream(file);
        ftpSession.read(inputUrl, outputStream);
        return outputStream;
    }

    @Override
    public void read(String s, OutputStream outputStream) throws IOException {

    }

    @Override
    public void mkdir(String path) throws IOException {
        this.recursiveMkdir(path);
//        ftpSession.mkdir(path);
    }

    @Override
    public boolean hasDir(String path) throws IOException {
        return ftpSession.exists(path);
    }

    @Override
    public void setFile(InputStream inputStream, String path) throws IOException {
        recursiveMkdir(path);
        ftpSession.write(inputStream, path);
        inputStream.close();
    }

    /**
     * Recursively make directories according to a path.
     *
     * @param path string containing the path
     * @throws IOException when the file cannot be accessed
     */
    private void recursiveMkdir(String path) throws IOException {
        String[] pathElements = path.split("/");
        String curDir = "";

        if (pathElements.length > 0) {

            for (int i=0; i < pathElements.length - 1; i++) {
                String singleDir = pathElements[i];

                if (singleDir.equals("~")) {
                    curDir = "~";
                } else {
                    curDir = curDir + "/" + singleDir;

                    if (!ftpSession.exists(curDir)) {
                        if (ftpSession.mkdir(curDir)) {
                            logger.info("Created directory: " + curDir);
                        } else {
                            logger.warn("Could not create directory: " + curDir);
                            break;
                        }
                    }
                }
            }

        }
    }
}
