/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.io;

import com.olifarm.olive.io.FilenameService;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Service used for creating the file and folder names for files on the ftp server.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class NameServiceImpl implements FilenameService {

    @Value("${ftp.external_storage_root}")
    private String REMOTE_STORAGE_ROOT;

    @Value("${system.local_storage_root}")
    private String LOCAL_STORAGE_ROOT;

    @Override
    public String makeLocalInputDir(Job job) {
        String username = job.getUsername();
        String externalJobRoot = LOCAL_STORAGE_ROOT + "/" + username.toLowerCase() + "/" + job.getId();
        return externalJobRoot + "/input";

    }

    @Override
    public String makeLocalOutputDir(Job job) {
        String externalJobRoot = LOCAL_STORAGE_ROOT + "/" + job.getUsername().toLowerCase() + "/" + job.getId();
        return externalJobRoot + "/output";
    }

    @Override
    public String makeRemoteInputDir(Job job) {
        String username = job.getUsername();
        String externalJobRoot = REMOTE_STORAGE_ROOT + "/" + username.toLowerCase() + "/" + job.getId();
        return externalJobRoot + "/input";
    }

    @Override
    public String makeRemoteOutputDir(Job job) {
        String externalJobRoot = REMOTE_STORAGE_ROOT + "/" + job.getUsername().toLowerCase() + "/" + job.getId();
        return externalJobRoot + "/output";
    }

    @Override
    public String createFilename(Job job, String extension, int fileCount) {
        String baseFileName = this.createBaseFileName(job);

        if (fileCount != 0) {
            baseFileName += "[" + fileCount + "]";
        }

        return baseFileName + "." + extension;
    }


    /**
     * Create the file paths for the output files for a job.
     *
     * @param job the job to create the output paths for
     * @return a list of parameter objects with the new file paths
     */
    public List<Parameter> addRemotePathToOutputParams(Job job) {
        List<Parameter> outputParams = new ArrayList<>();
        for (Parameter parameter : job.getApp().getParameters()) {

            if (    parameter.getType() == ParameterType.OUTPUT_FILE ||
                    parameter.getType() == ParameterType.STD_OUT ||
                    parameter.getType() == ParameterType.STD_ERR) {

                String fileName = this.createBaseFileName(job) + "." + parameter.getExtension();
                parameter.setFilename(fileName);

                String completePath = this.makeRemoteOutputDir(job) + "/" + fileName;

                parameter.setArguments(Collections.singletonList(completePath));

                outputParams.add(parameter);

            }
        }

        return outputParams;
    }

    /**
     * Create the base filename for every file created for this job.
     * @param job the job to create the filename for
     * @return the base filename
     */
    private String createBaseFileName(Job job) {
        return job.getApp().getName().toLowerCase() + "_job-" + job.getId();
    }

}
