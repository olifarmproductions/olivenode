/*
 *     OliveNode is the node application in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.node.system;

import org.springframework.stereotype.Service;

/**
 * Service used for checking if the system is available to receive a new job.
 *
 * @author Harm Brugge
 * @author Oliver Bakker
 */
@Service
public class SystemStatusChecker {

    /**
     * Checks the systems status.
     *
     * @return the systems status
     */
    public SystemStatus checkStatus() {
        // Not yet implemented
        return SystemStatus.AVAILABLE;
    }
}
